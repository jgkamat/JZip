JZip
================

JZip is a revolutionary implementation of LZ77 that is obviously superior to all other compression programs out there!

JZip is the only known semi-popular ziping program that does not inflate empty files!
