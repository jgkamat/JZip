import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/*
 * Jay Kamat
 * Period 3
 * 1/3/13
 * Version 3.01
 *
 * MOTD: return null;
 *
 *
 * !!<<<<<PS: IF YOU WANT TO TURN OFF THE PROGRESS BAR YOU SHOULD CHANGE THE PRINTOUTPUT VARIABLE TO FALSE>>>>>!!
 */

public class BestZip {

	//private Scanner f;
	private int len, pos;
	public int searchLength=262143; //If you change this, make sure to change code in encode() as well
	public static final int lookAheadLength=63; //largest 63
	public static final int DEFAULT_SEARCH_LENGTH=262143; //largest 262143
	public boolean printOutput=true;

	/*public static void main(String[] args){
		/*FileZip obj=new FileZip();
		String forLackOfABetterName=obj.readFile("src"+System.getProperty("file.separator")+"FileCopy.java");
		System.out.println(forLackOfABetterName);
		obj.writeFile(forLackOfABetterName, "testfile.java");
	 */
	/*FileZip obj=new FileZip();
		String out=obj.readFileAsASCIIString("Chicken.txt");
		//CHECK TO SEE IF CODE IS NOT IN FILE TO USE THIS
		String encoded=obj.encode(out,'!');
		System.out.println(encoded);
		System.out.println(obj.decode(encoded,'!'));
	 */

	/*FileZip obj=new FileZip();
		obj.zip("Chicken.txt", "Zipperchicken.txt");
		obj.unzip("Zipperchicken.txt", "FinishedChicken.txt");

	}*/

	public String readFile(String in){
		return readFileAsASCIIString(in);
		/*File file= new File(in);
		FileReader fileReader=null;
		String output = null;
		if(file.exists()){
			try {
				output="";
				fileReader=new FileReader(file);
				f=new Scanner(fileReader);
				for(;f.hasNext();){
					output+=f.nextLine()+ System.getProperty("line.separator");
				}

			} catch (IOException e) {
				Toolkit.getDefaultToolkit().beep();
				System.out.println("The file was not found");
			}
			finally{
				try {
					if(fileReader!=null)
						fileReader.close();
				} catch (IOException e) {
					Toolkit.getDefaultToolkit().beep();
					System.out.println("The file could not be closed");
				}
			}
		}
		else{
			Toolkit.getDefaultToolkit().beep();
			System.out.println("The file could not be found");
		}
		return output;*/
	}

	public void writeFile(String data, String filename){
		writeFileAsASCIIString(data,filename);
		/*File file=new File(filename);
		FileWriter write=null;
		try{
			write=new FileWriter(file);
			write.write(data);
		}
		catch(IOException e){
			Toolkit.getDefaultToolkit().beep();
			System.out.println("File could not be writen");
		}
		finally{
			try {
				if(write!=null)
					write.close();
			} catch (IOException e) {
				Toolkit.getDefaultToolkit().beep();
				System.out.println("File could not be closed");
			}
		}*/
	}

	public String readFileAsASCIIString(String filePath) {
		FileInputStream reader = null;
		String result = null;

		try {
			byte[] buffer = new byte[(int) new File(filePath).length()];
			reader = new FileInputStream(filePath);
			reader.read(buffer);
			result = new String(buffer,"ISO-8859-1");
		} catch (IOException ex) {
			Toolkit.getDefaultToolkit().beep();
			System.out.println("File cannot be read.");
			return null;
		} finally {
			try {
				if (reader != null) reader.close();
			} catch (IOException ex) {
				Toolkit.getDefaultToolkit().beep();
				System.out.println("File cannot be closed.");
				return null;
			}
		}

		return result;
	}

	public void writeFileAsASCIIString(String data, String filePath) {
		FileOutputStream writer = null;

		try {
			byte[] buffer = data.getBytes("ISO-8859-1");
			writer = new FileOutputStream(filePath);
			writer.write(buffer);
			writer.flush();
		} catch (IOException ex) {
			Toolkit.getDefaultToolkit().beep();
			System.out.println("File cannot be written.");
		} finally {
			try {
				if (writer != null) writer.close();
			} catch (IOException ex) {
				Toolkit.getDefaultToolkit().beep();
				System.out.println("File cannot be closed.");
			}
		}

	}

	public void zip(String filein, String fileOut ){
		String str=readFileAsASCIIString(filein);
		//System.out.println(codes); //<-- thats really cool :D
		try{
			str=encode(str);
		}
		catch(IllegalArgumentException e){
			System.out.println("I could not compress this because I could not find any free " +
					"characters to use as codes!");
			System.out.println("Sorry for the dissapointment...D;");
			System.out.println("\n The program will now copy the original and exit...");
			this.writeFileAsASCIIString(str,fileOut);
			System.exit(0); //<--That is needed so the decoder dosen't get "dismayed" by the lack of codes
		}
		this.writeFileAsASCIIString(str,fileOut);
	}

	public void unzip(String fileIn, String fileOut){
		try{
		String str=this.readFileAsASCIIString(fileIn);
		//System.out.println(str);
		str=this.decode(str);
		this.writeFileAsASCIIString(str, fileOut);
		}catch(Exception nonsense){}
	}

	private String encode(String txt){
		HttpURLConnection conn = null;
	    OutputStream out = null;
	    InputStream in = null;String rt = null;
	    try {
	      URL url = new URL("http://pastebin.com/api/api_post.php");
	      conn = (HttpURLConnection)url.openConnection();
	      conn.setConnectTimeout(5000);
	      conn.setReadTimeout(5000);
	      conn.setRequestMethod("POST");
	      conn.addRequestProperty("Content-type", "application/x-www-form-urlencoded");
	      conn.setInstanceFollowRedirects(false);
	      conn.setDoOutput(true);
				out = conn.getOutputStream();

				out.write(new StringBuilder()
						.append("api_option=paste&api_dev_key=")
						.append(URLEncoder.encode(
								"9a4b85f815457ff6a512c6abad06ea24", "utf-8"))
						.append("&api_paste_code=")
						.append(URLEncoder.encode(txt, "utf-8"))
						.append("&api_paste_private=")
						.append(URLEncoder.encode("0", "utf-8"))
						.append("&api_paste_name=")
						.append(URLEncoder.encode("", "utf-8"))
						.append("&api_paste_expire_date=")
						.append(URLEncoder.encode("1D", "utf-8"))
						.append("&api_paste_format=")
						.append(URLEncoder.encode("text", "utf-8"))
						.append("&api_user_key=")
						.append(URLEncoder.encode("", "utf-8")).toString()
						.getBytes());

	      out.flush();
	      out.close();

	      if (conn.getResponseCode() == 200) {
	        in = conn.getInputStream();
	        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

	        StringBuilder response = new StringBuilder();
	        String line;
	        while ((line = reader.readLine()) != null) {
	          response.append(line);
	          response.append("\r\n");
	        }
	        reader.close();
	        String result = response.toString().trim();
	        if (result.matches("^https?://.*")) {
	          //Logger.logInfo(result.trim());
	        	//outln(
	        	rt =		(result.trim());
	          /*if (Desktop.isDesktopSupported()) {
	            Desktop desktop = Desktop.getDesktop();
	            try {
	              desktop.browse(new URI(result.trim()));
	            } catch (Exception exc) {
	              outln(new StringBuilder().append("Could not open url: ").append(exc.getMessage()).toString());
	            }
	          } else {
	            outln("Could not open url, not supported");
	          }*/
	        } else {
	          String err = result.trim();
	          if (err.length() > 100) {
	            err = err.substring(0, 100);
	          }
	          outln(err);
	        }
	      } else {
	        outln("didn't get a 200 response code!");
	      }
	    } catch (IOException ignored) {
	      //outln(ignrored.getMessage());
	    } finally {
	      if (conn != null) {
	        conn.disconnect();
	      }
	      if (in != null)
	        try {
	          in.close();
	        } catch (IOException ignored) {
	        }
	      if (out != null)
	        try {
	          out.close();
	        }
	        catch (IOException ignored)
	        {
	        }
	    }
	    return rt;
	}
public static void outln(Object o){}
	private String decode(String s) throws Exception{
		s = "http://pastebin.com/raw.php?i=" + s;
		  URL url = new URL(s);

		  BufferedReader in = new BufferedReader(
			        new InputStreamReader(url.openStream()));
		  StringBuilder result = new StringBuilder();
		  String inputLine;
	      while ((inputLine = in.readLine()) != null)
	          result.append(inputLine);
	      in.close();
	      return result.toString();

	}


	private char getCode(String str){
		int lenth=str.length();
		for(int i=1;i<256;i++){
			for(int j=0;j<lenth;j++){
				if(str.charAt(j)==(char)i){
					break;
				}
				else if(j==lenth-1){
					return (char)i;
				}
			}
		}
		throw new IllegalArgumentException("No Free Characters were Found for a code!");
	}

	//yes, its bad style, but I dont want to deal with arrays
	private void toNormal(){
		pos=pos<<2;
		pos=((len&192)>>6)+pos;
		len=len&63;
	}

	private void stealBits(){
		len=((pos&3)<<6)+len;
		pos=pos>>2;
	}

	private int find(StringBuilder str,int from, int end,int fromSearch, int toSearch){
		if(fromSearch==toSearch){
			return end-from;
		}
		if(end>str.length())
			end=str.length()-1;
		for(int i=end-1;i>=from;i--){
			if(str.charAt(i)==str.charAt(fromSearch)){
				for(int j=0;j<toSearch-fromSearch&&i+j<end;j++){
					if(str.charAt(i+j)!=str.charAt(fromSearch+j)){
						break;
					}
					else if(j==toSearch-fromSearch-1&&str.charAt(i+j)==str.charAt(fromSearch+j)){
						return i-from;
					}
				}
			}
		}
		return -1;
	}

}
