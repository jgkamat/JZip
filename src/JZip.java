import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/*
 * Jay Kamat
 * Period 3
 * 1/3/13
 * Version 3.01
 *
 * MOTD: return null;
 *
 *
 * !!<<<<<PS: IF YOU WANT TO TURN OFF THE PROGRESS BAR YOU SHOULD CHANGE THE PRINTOUTPUT VARIABLE TO FALSE>>>>>!!
 */

public class JZip {

	//private Scanner f;
	private int len, pos;
	public int searchLength=262143; //If you change this, make sure to change code in encode() as well
	public static final int lookAheadLength=63; //largest 63
	public static final int DEFAULT_SEARCH_LENGTH=262143; //largest 262143
	public boolean printOutput=true;

	/*public static void main(String[] args){
		/*FileZip obj=new FileZip();
		String forLackOfABetterName=obj.readFile("src"+System.getProperty("file.separator")+"FileCopy.java");
		System.out.println(forLackOfABetterName);
		obj.writeFile(forLackOfABetterName, "testfile.java");
	 */
	/*FileZip obj=new FileZip();
		String out=obj.readFileAsASCIIString("Chicken.txt");
		//CHECK TO SEE IF CODE IS NOT IN FILE TO USE THIS
		String encoded=obj.encode(out,'!');
		System.out.println(encoded);
		System.out.println(obj.decode(encoded,'!'));
	 */

	/*FileZip obj=new FileZip();
		obj.zip("Chicken.txt", "Zipperchicken.txt");
		obj.unzip("Zipperchicken.txt", "FinishedChicken.txt");

	}*/

	public String readFile(String in){
		return readFileAsASCIIString(in);
		/*File file= new File(in);
		FileReader fileReader=null;
		String output = null;
		if(file.exists()){
			try {
				output="";
				fileReader=new FileReader(file);
				f=new Scanner(fileReader);
				for(;f.hasNext();){
					output+=f.nextLine()+ System.getProperty("line.separator");
				}

			} catch (IOException e) {
				Toolkit.getDefaultToolkit().beep();
				System.out.println("The file was not found");
			}
			finally{
				try {
					if(fileReader!=null)
						fileReader.close();
				} catch (IOException e) {
					Toolkit.getDefaultToolkit().beep();
					System.out.println("The file could not be closed");
				}
			}
		}
		else{
			Toolkit.getDefaultToolkit().beep();
			System.out.println("The file could not be found");
		}
		return output;*/
	}

	public void writeFile(String data, String filename){
		writeFileAsASCIIString(data,filename);
		/*File file=new File(filename);
		FileWriter write=null;
		try{
			write=new FileWriter(file);
			write.write(data);
		}
		catch(IOException e){
			Toolkit.getDefaultToolkit().beep();
			System.out.println("File could not be writen");
		}
		finally{
			try {
				if(write!=null)
					write.close();
			} catch (IOException e) {
				Toolkit.getDefaultToolkit().beep();
				System.out.println("File could not be closed");
			}
		}*/
	}

	public String readFileAsASCIIString(String filePath) {
		FileInputStream reader = null;
		String result = null;

		try {
			byte[] buffer = new byte[(int) new File(filePath).length()];
			reader = new FileInputStream(filePath);
			reader.read(buffer);
			result = new String(buffer,"ISO-8859-1");
		} catch (IOException ex) {
			Toolkit.getDefaultToolkit().beep();
			System.out.println("File cannot be read.");
			return null;
		} finally {
			try {
				if (reader != null) reader.close();
			} catch (IOException ex) {
				Toolkit.getDefaultToolkit().beep();
				System.out.println("File cannot be closed.");
				return null;
			}
		}

		return result;
	}

	public void writeFileAsASCIIString(String data, String filePath) {
		FileOutputStream writer = null;

		try {
			byte[] buffer = data.getBytes("ISO-8859-1");
			writer = new FileOutputStream(filePath);
			writer.write(buffer);
			writer.flush();
		} catch (IOException ex) {
			Toolkit.getDefaultToolkit().beep();
			System.out.println("File cannot be written.");
		} finally {
			try {
				if (writer != null) writer.close();
			} catch (IOException ex) {
				Toolkit.getDefaultToolkit().beep();
				System.out.println("File cannot be closed.");
			}
		}

	}

	public void zip(String filein, String fileOut ){
		String str=readFileAsASCIIString(filein);
		//System.out.println(codes); //<-- thats really cool :D
		try{
			str=encode(str);
		}
		catch(IllegalArgumentException e){
			System.out.println("I could not compress this because I could not find any free " +
					"characters to use as codes!");
			System.out.println("Sorry for the dissapointment...D;");
			System.out.println("\n The program will now copy the original and exit...");
			this.writeFileAsASCIIString(str,fileOut);
			System.exit(0); //<--That is needed so the decoder dosen't get "dismayed" by the lack of codes
		}
		this.writeFileAsASCIIString(str,fileOut);
	}

	public void unzip(String fileIn, String fileOut){
		String str=this.readFileAsASCIIString(fileIn);
		//System.out.println(str);
		str=this.decode(str);
		this.writeFileAsASCIIString(str, fileOut);
	}

	private String encode(String str2){
		int previousLen=searchLength;
		int oldPercent=-1;
		char code=getCode(str2);
		char code2;
		int looking;
		int previousPos;
		try{
			code2=getCode(code+""+str2);
		}
		catch(IllegalArgumentException e){
			System.out.println("\nCongratulations ! You have only 1 free ASCII character!\n" +
					"This program will now go into backup mode...");
			printOutput=false; //so thats^ more obvious
			searchLength=1023;
			code2=(char)0; //This like a error report to the decoder
		}

		if(str2.length()<searchLength){
			searchLength=str2.length();
		}

		StringBuilder str=new StringBuilder(str2);
		StringBuilder out=new StringBuilder(searchLength+str.length());

		StringBuilder fillerSpace=new StringBuilder(" ");
		for(int i=0;fillerSpace.length()<searchLength+1;i++){
			fillerSpace.append(fillerSpace);
		}
		fillerSpace.delete(searchLength+1,fillerSpace.length());

		str.insert(0, fillerSpace);

		for(int i=searchLength+1;i<str.length();){
			int amountDone=(int) (100*(double)(i-searchLength+1)/(str.length()-searchLength+1));
			if(oldPercent!=amountDone&&printOutput){
				System.out.println(amountDone+"% zipped");
				oldPercent=amountDone;
			}
			len=0;
			pos=0;
			previousPos=i;
			for(int j=i+1;j<i+lookAheadLength;j++){
				looking=find(str,i-searchLength,previousPos,i,j);
				previousPos=Math.min(i-searchLength+looking+j-i+1,i);
				if(j>=str.length()-1||looking==-1
						||j-i-1>=lookAheadLength){
					pos=find(str,i-searchLength,i,i,j-1);
					pos=searchLength-pos;
					len=j-i-1;
					break;
				}
			}
			if(len>4&&pos>1023){
				int realLen=len;
				int toWrite=((255&pos));
				pos=pos>>8;
				stealBits();
				out.append(code2+""+(char)pos+(char)toWrite+(char)len);
				i+=realLen;
			}
			else if(len>3&&pos>-1&&pos<=1023){
				int realLen=len;
				stealBits();
				out.append(code+""+(char)pos+(char)len);
				i+=realLen;
			}
			else{
				out.append(str.charAt(i));
				i++;
			}
		}
		searchLength=DEFAULT_SEARCH_LENGTH; //so I dont "cheat"...
		return code+""+code2+out.toString();
	}

	private String decode(String str2){
		int oldPercent=-1;
		StringBuilder str=new StringBuilder(str2);
		char code=str.charAt(0);
		char code2=str.charAt(1);
		str.deleteCharAt(0);
		str.deleteCharAt(0);

		if(code2==(char)0){
			code2=(char)Integer.MAX_VALUE;
			printOutput=false;
		}

		StringBuilder fillerSpace=new StringBuilder(" ");
		for(int i=0;fillerSpace.length()<searchLength+1;i++){
			fillerSpace.append(fillerSpace);
		}
		fillerSpace.delete(searchLength+1,fillerSpace.length());

		str.insert(0, fillerSpace);

		for(int i=searchLength+1;i<str.length();i++){
			int amountDone=(int) (100*(double)(i-searchLength+1)/(str.length()-searchLength+1));
			if(oldPercent!=amountDone&&printOutput){
				System.out.println(amountDone+"% unzipped");
				oldPercent=amountDone;
			}
			if(str.charAt(i)==code2){
				pos=(int)str.charAt(i+1);
				int toWrite=(int)str.charAt(i+2);
				len=(int)str.charAt(i+3);
				toNormal();
				pos=(pos<<8)+toWrite;
				str.delete(i, i+4);
				str.insert(i,str.substring(i-pos,i-pos+len));
			}
			else if(str.charAt(i)==code){
				pos=(int)str.charAt(i+1);
				len=(int)str.charAt(i+2);
				toNormal();
				//System.out.println(pos+", "+len);
				str.delete(i, i+3);
				str.insert(i,str.substring(i-pos,i-pos+len));
			}
		}

		return str.substring(searchLength+1);
	}


	private char getCode(String str){
		int lenth=str.length();
		for(int i=1;i<256;i++){
			for(int j=0;j<lenth;j++){
				if(str.charAt(j)==(char)i){
					break;
				}
				else if(j==lenth-1){
					return (char)i;
				}
			}
		}
		throw new IllegalArgumentException("No Free Characters were Found for a code!");
	}

	//yes, its bad style, but I dont want to deal with arrays
	private void toNormal(){
		pos=pos<<2;
		pos=((len&192)>>6)+pos;
		len=len&63;
	}

	private void stealBits(){
		len=((pos&3)<<6)+len;
		pos=pos>>2;
	}

	private int find(StringBuilder str,int from, int end,int fromSearch, int toSearch){
		if(fromSearch==toSearch){
			return end-from;
		}
		if(end>str.length())
			end=str.length()-1;
		for(int i=end-1;i>=from;i--){
			if(str.charAt(i)==str.charAt(fromSearch)){
				for(int j=0;j<toSearch-fromSearch&&i+j<end;j++){
					if(str.charAt(i+j)!=str.charAt(fromSearch+j)){
						break;
					}
					else if(j==toSearch-fromSearch-1&&str.charAt(i+j)==str.charAt(fromSearch+j)){
						return i-from;
					}
				}
			}
		}
		return -1;
	}

}
